
# User specific aliases and functions
PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\W\[\033[1;31m\]\$\[\033[0m\] '
LS_COLORS="di=00;38;5;12:ln=00;38;5;2:ex=01;38;5;14:"
alias ls='ls --color=always'
export PYTHONWARNINGS="ignore"
#alias rm='rm -i'
