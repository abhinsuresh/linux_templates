set number
syntax on
set nowrap
colorscheme black_angus 
set tabstop=4 shiftwidth=4 expandtab
set colorcolumn=69
highlight ColorColumn ctermbg=gray
set viminfo='100,<1000,s100,h
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif
