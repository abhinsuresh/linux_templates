#!bin/bash
if [ -d  "$HOME/.vim/colors" ]; then
    echo ".vim/colors directories exists"
else
    echo "Making $HOME/.vim/colors"
    mkdir -p $HOME/.vim/colors
fi
cat .bashrc >> $HOME/.bashrc
cat .vimrc >> $HOME/.vimrc
cat .tmux.conf >> $HOME/.tmux.conf
cp black_angus.vim $HOME/.vim/colors/.

